<?php 
require_once "connexionPDO.php";


/********** CLASSE page *************/
Class Page{
	/*Toutes les pages*/
	public function getPage(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM page");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}
}
$Page = new Page();
$lesPages = $Page->getPage();


/********** CLASSE lieu *************/
Class Lieu{

	/*Tous les lieux*/
	public function getLieux(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM lieu");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}


	/*Lieu par Id*/
	public function getLieuById($id){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM lieu WHERE id_lieu = ".$id);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Les communes définient sur un interval*/
	public function getLimitCommunes($limitA,$limitB){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM lieu WHERE photo_lieu IS NOT NULL LIMIT ".$limitA.",".$limitB);
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

}
$Lieu = new Lieu();
$lesLieux = $Lieu->getLieux();


/********** CLASSE categorie *************/
Class Categorie{

	/*Toutes les categories*/
	public function getCategorie(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM categorie");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Les catégories limités*/
	public function getLimitCategories($limitA,$limitB){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM categorie LIMIT ".$limitA.",".$limitB);
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Nombre de toutes les categories*/
	public function getNbCategorie(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT count(*) AS nombreCat FROM categorie");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/* catégorie pour une annonce*/
	public function getCategorieByAnnonce($id_annonce){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM categorie C INNER JOIN annon_cat_souscat ACS ON C.id_cat = ACS.id_cat WHERE id_annonce = ".$id_annonce);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/* catégorie par Id*/
	public function getCategorieByid($id_cat){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM categorie WHERE id_cat = ".$id_cat);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

}
$Categorie = new Categorie();
$lesCategories = $Categorie->getCategorie();
$nbCategories = $Categorie->getNbCategorie()['nombreCat'];


/********** CLASSE sous_categorie *************/
Class SousCategorie{

	/*Toutes les sous categories*/
	public function getSousCategorie(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM sous_categorie");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Sous catégorie pour une annonce*/
	public function getSousCategorieByAnnonce($id_annonce){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM sous_categorie SC INNER JOIN annon_cat_souscat ACS ON SC.id_sous_cat = ACS.id_sous_cat WHERE id_annonce = ".$id_annonce);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Toutes les sous categories par catégorie*/
	public function getSousCategorieByIdCat($id_cat){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM sous_categorie WHERE id_cat =".$id_cat);
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}



	/*Une sous categorie*/
	public function getSousCategorieByIdSousCat($id_sous_cat){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM sous_categorie WHERE id_sous_cat =".$id_sous_cat);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

}
$SousCategorie = new SousCategorie();
$lesSousCategories = $SousCategorie->getSousCategorie();


/********** CLASSE situation *************/
Class Situation{

	/*Toutes les situations*/
	public function getSituation(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM situtation");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Situation By Id*/
	public function getSituationById($id){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM situtation WHERE id_situation = ". $id);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

}
$Situation = new Situation();
$lesSituations = $Situation->getSituation();


/********** CLASSE type_etablissement *************/
Class TypeEtablissement{

	/*Tous les types d'établissement*/
	public function getTypeEtablissement(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM type_etablissement");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

}
$TypeEtablissement = new TypeEtablissement();
$lesTypeEtablissements = $TypeEtablissement->getTypeEtablissement();


/********** CLASSE note *************/
Class Note{

	/*Toutes les notes*/
	public function getNote(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM note");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

}
$Note = new Note();
$lesNotes = $Note->getNote();


/********** CLASSE noter *************/
Class Noter{

	/*Toutes les notées*/
	public function getNoter($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM noter WHERE id_annonce = 1 AND id_prestataire = ".$id_user);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Toutes les notées*/
	public function getNbNoter($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT count(*) AS nbnote, AVG(note_utilisateur2) AS mynnote FROM noter WHERE id_annonce = 1 AND id_prestataire = ".$id_user);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

}
$Noter = new Noter();


/********** CLASSE jour *************/
Class Jour{

	/*Tous les jours*/
	public function getJour(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM jour");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

}
$Jour = new Jour();
$lesJours = $Jour->getJour();


/********** CLASSE type_tarification *************/
Class TypeTarification{

	/*Toutes les types de tarification*/
	public function getTypeTarification(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM type_tarification");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Type tarification par id*/
	public function getTypeTarificationById($id_type_tarif){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM type_tarification WHERE id_type_tarif = ".$id_type_tarif);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

}
$TypeTarification = new TypeTarification();
$lesTypeTarifications = $TypeTarification->getTypeTarification();


/********** CLASSE competence *************/
Class Competence{

	/*Toutes les competences*/
	public function getCompetence(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM competence");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

}
$Competence = new Competence();
$lesCompetences = $Competence->getCompetence();



/********** CLASSE competence *************/
Class Competence_Utilisateur{

	/*Toutes les competences par utilisateur*/
	public function getCompetence_UtilisateurById($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM competence_utilisateur CU INNER JOIN competence C ON CU.id_competence = C.id_competence WHERE id_utilisateur = ".$id_user);
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}
	/*Toutes les competences par utilisateur*/
	public function getCompetence_Utilisateur(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM competence_utilisateur");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}


	/*Suppression des compétences BY IdUser*/
	public function deleteComptence_Utilisateur($id_utilisateur){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("DELETE FROM `competence_utilisateur` WHERE `id_utilisateur` = ".$id_utilisateur);
		$req->execute(); 
	}

	/*Insertion des compétences BY IdUser*/
	public function setComptence_Utilisateur($donnees){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$sql = "INSERT INTO `competence_utilisateur` (`id_utilisateur`, `id_competence`) VALUES ".$donnees;

		$insert_Comptence_Utilisateur = $connect->prepare($sql);
		$insert_Comptence_Utilisateur->execute();
	}
}
$Competence_Utilisateur = new Competence_Utilisateur();
$lesCompetence_Utilisateurs = $Competence_Utilisateur->getCompetence_Utilisateur();


/********** CLASSE annonce *************/
Class Annonce{

	/*Toutes les annonces*/
	public function getAnnonce(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM annonce ORDER BY date_pub DESC");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Toutes les annonces by user */
	public function getAnnonceByIdUser($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM `annonce` WHERE id_utilisateur = ".$id_user);
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}


	/*Format date publication*/
	public function getAnnonceById($id_annonce){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT *, DATE_FORMAT(date_debut, '%d %M %Y') AS dtedebut, DATE_FORMAT(date_fin, '%d %M %Y') AS dtefin FROM annonce WHERE id_annonce = ".$id_annonce);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/* Annonces en interval */
	public function getAnnonceByPage($id_lieu, $id_cat, $id_sous_cat, $interval1, $interval2){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$lesql = "";

		
		if (!empty($id_lieu) && !empty($id_cat) && !empty($id_sous_cat)) {//Si on filtre tout
			$lesql = "WHERE id_lieu = ".$id_lieu." AND id_cat = ".$id_cat." AND id_sous_cat = ".$id_sous_cat;
		}elseif (!empty($id_lieu) && !empty($id_cat) && empty($id_sous_cat)) {//Si on filtre lieu et cat
			$lesql = "WHERE id_lieu = ".$id_lieu." AND id_cat = ".$id_cat;
		}elseif (!empty($id_lieu) && empty($id_cat) && !empty($id_sous_cat)) {//Si on filtre lieu et sous_cat
			$lesql = "WHERE id_lieu = ".$id_lieu." AND id_sous_cat = ".$id_sous_cat;
		}elseif (empty($id_lieu) && !empty($id_cat) && !empty($id_sous_cat)) {//Si on filtre cat et sous_cat
			$lesql = "WHERE id_cat = ".$id_cat." AND id_sous_cat = ".$id_sous_cat;
		}elseif (!empty($id_lieu) && empty($id_cat) && empty($id_sous_cat)) {//Si on filtre que lieu
			$lesql = "WHERE id_lieu = ".$id_lieu;
		}elseif (empty($id_lieu) && !empty($id_cat) && empty($id_sous_cat)) {//Si on filtre que cat
			$lesql = "WHERE id_cat = ".$id_cat;
		}elseif (empty($id_lieu) && empty($id_cat) && !empty($id_sous_cat)) {//Si on filtre que sous_cat
			$lesql = "WHERE id_sous_cat = ".$id_sous_cat;
		}else{//Sinon 
			if (!empty($id_lieu) || !empty($id_cat) || !empty($id_sous_cat)) {//S'il y a des données
				$lesql = "WHERE A.id_annonce = 0";
			}
		}

		$sql = "SELECT * FROM annonce A INNER JOIN annon_cat_souscat ACS ON A.id_annonce = ACS.id_annonce ".$lesql." ORDER BY date_pub DESC LIMIT ".$interval1.",".$interval2;

		$req = $connect->prepare($sql);
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}


	/* 4 dernières annonces pour  */
	public function getAnnonceByQuatreDernieres($id_annonce, $id_sous_cat){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM annonce A INNER JOIN annon_cat_souscat ACS ON A.id_annonce = ACS.id_annonce WHERE id_sous_cat = ".$id_sous_cat." AND A.id_annonce != ".$id_annonce." ORDER BY date_pub ASC LIMIT 4");

		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}


	/*Nombre d'annonce */
	public function getNbAnnonce(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT count(A.id_annonce) AS nbAnnonce FROM annonce A INNER JOIN annon_cat_souscat ACS ON A.id_annonce = ACS.id_annonce");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/* Nombre d'annonce par page*/
	public function getNbAnnonceByPage($id_lieu, $id_cat, $id_sous_cat){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$lesql = "";

		
		if (!empty($id_lieu) && !empty($id_cat) && !empty($id_sous_cat)) {//Si on filtre tout
			$lesql = "WHERE id_lieu = ".$id_lieu." AND id_cat = ".$id_cat." AND id_sous_cat = ".$id_sous_cat;
		}elseif (!empty($id_lieu) && !empty($id_cat) && empty($id_sous_cat)) {//Si on filtre lieu et cat
			$lesql = "WHERE id_lieu = ".$id_lieu." AND id_cat = ".$id_cat;
		}elseif (!empty($id_lieu) && empty($id_cat) && !empty($id_sous_cat)) {//Si on filtre lieu et sous_cat
			$lesql = "WHERE id_lieu = ".$id_lieu." AND id_sous_cat = ".$id_sous_cat;
		}elseif (empty($id_lieu) && !empty($id_cat) && !empty($id_sous_cat)) {//Si on filtre cat et sous_cat
			$lesql = "WHERE id_cat = ".$id_cat." AND id_sous_cat = ".$id_sous_cat;
		}elseif (!empty($id_lieu) && empty($id_cat) && empty($id_sous_cat)) {//Si on filtre que lieu
			$lesql = "WHERE id_lieu = ".$id_lieu;
		}elseif (empty($id_lieu) && !empty($id_cat) && empty($id_sous_cat)) {//Si on filtre que cat
			$lesql = "WHERE id_cat = ".$id_cat;
		}elseif (empty($id_lieu) && empty($id_cat) && !empty($id_sous_cat)) {//Si on filtre que sous_cat
			$lesql = "WHERE id_sous_cat = ".$id_sous_cat;
		}else{//Sinon 
			if (!empty($id_lieu) || !empty($id_cat) || !empty($id_sous_cat)) {//S'il y a des données
				$lesql = "WHERE A.id_annonce = 0";
			}
		}

		$sql = "SELECT count(A.id_annonce) AS nbAnnonce FROM annonce A INNER JOIN annon_cat_souscat ACS ON A.id_annonce = ACS.id_annonce ".$lesql;

		$req = $connect->prepare($sql);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Nombre d'annonce par catégorie*/
	public function getNbAnnonceByCat($id_cat){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT count(ACS.id_annonce) AS nbAnnonce FROM annonce A INNER JOIN annon_cat_souscat ACS ON A.id_annonce = ACS.id_annonce WHERE id_cat = ".$id_cat);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}



	/*Format date publication*/
	public function getFormatDateAnnoncePub($id_annonce){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT DATEDIFF(NOW(), date_pub) AS dtediff, DATE_FORMAT(date_pub, '%a %e %b %Y à %H:%i') AS date_annee, DATE_FORMAT(date_pub, '%a %e %b à %H:%i') AS date_mois, DATE_FORMAT(date_pub, '%a à %H:%i') AS date_semaine, DATE_FORMAT(date_pub, '%H:%i') AS date_jour FROM annonce WHERE id_annonce = ".$id_annonce);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}


	/*La dernière annonce */
	public function getDerniereAnnonce(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM annonce ORDER BY id_annonce DESC LIMIT 1");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}


	/*Insertion dans la base de données*/
	public function setAnnonce($lib_annon, $description, $adresse, $dte_debut, $dte_fin, $prix, $id_type_tarif, $id_user, $id_user_reserver, $id_lieu, $id_type_service, $tel){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_Annonce = $connect->prepare("INSERT INTO `annonce`(`lib_annonce`, `description`, `adresse`, `date_debut`, `date_fin`, `prix`, `date_pub`, `id_type_tarif`, `id_utilisateur`, `id_utilisateur_reserver`, `id_lieu`, id_type_service, tel_annonce) VALUES (:lib_annonce, :description, :adresse, :dte_debut, :dte_fin, :prix, NOW(),  :id_type_tarif, :id_user, NULL, :id_lieu, :id_type_service, :tel)");
		$insert_Annonce->bindParam(':lib_annonce', $lib_annon);
		$insert_Annonce->bindParam(':description', $description);
		$insert_Annonce->bindParam(':adresse', $adresse);
		$insert_Annonce->bindParam(':dte_debut', $dte_debut);
		$insert_Annonce->bindParam(':dte_fin', $dte_fin);
		$insert_Annonce->bindParam(':prix', $prix);
		$insert_Annonce->bindParam(':id_type_tarif', $id_type_tarif);
		$insert_Annonce->bindParam(':id_user', $id_user);
		//$insert_Annonce->bindParam(':id_user_reserver', $id_user_reserver);
		$insert_Annonce->bindParam(':id_lieu', $id_lieu);
		$insert_Annonce->bindParam(':id_type_service', $id_type_service);
		$insert_Annonce->bindParam(':tel', $tel);
		$insert_Annonce->execute();
	}
}
$Annonce = new Annonce();
$lesAnnonces = $Annonce->getAnnonce();
$nbAnnonces = $Annonce->getNbAnnonce()['nbAnnonce'];



/********** CLASSE annoncecatsouscat *************/
Class AnnonCatSousCat{

	/*Insertion dans la base de données*/
	public function setAnnonCatSousCat($id_annonce, $id_cat, $id_sous_cat){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_Annonce = $connect->prepare("INSERT INTO `annon_cat_souscat`(`id_annonce`, `id_cat`, `id_sous_cat`) VALUES (:id_annonce,:id_cat,:id_sous_cat)");
		$insert_Annonce->bindParam(':id_annonce', $id_annonce);
		$insert_Annonce->bindParam(':id_cat', $id_cat);
		$insert_Annonce->bindParam(':id_sous_cat', $id_sous_cat);
		$insert_Annonce->execute();
	}
}
$AnnonCatSousCat = new AnnonCatSousCat();


/********** CLASSE planifier *************/
Class Planifier{

	/*Un jour planifier*/
	public function getPlanifierByIdJourAndAnnonce($id_jour, $id_annonce){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT *, DATE_FORMAT(`hdebut`, '%H:%i') AS h_debut, DATE_FORMAT(`hfin`, '%H:%i') AS h_fin FROM jour J INNER JOIN planifier P ON J.id_jour = P.id_jour WHERE J.id_jour = ".$id_jour." AND id_annonce = ".$id_annonce);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Insertion dans la base de données*/
	public function setPlanifier($id_annonce, $id_jour, $hdebut, $hfin){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_planifier = $connect->prepare("INSERT INTO `planifier`(`id_annonce`, `id_jour`, `hdebut`, `hfin`) VALUES (:id_annonce,:id_jour,:hdebut,:hfin)");
		$insert_planifier->bindParam(':id_annonce', $id_annonce);
		$insert_planifier->bindParam(':id_jour', $id_jour);
		$insert_planifier->bindParam(':hdebut', $hdebut);
		$insert_planifier->bindParam(':hfin', $hfin);
		$insert_planifier->execute();
	}
}
$Planifier = new Planifier();



/********** CLASSE utilisateur *************/
Class Utilisateur{

	/*Tous les utilisateurs*/
	public function getUtilisateur(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM utilisateur");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Le dernier utilisateur */
	/*public function getDerniereAnnonce(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM utilisateur ORDER BY id_utilisateur DESC LIMIT 1");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}*/

	/*Nombre d'utilisateur */
	public function getNbUtilisateur(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT count(id_annonce) AS nbUtilisateur FROM utilisateur");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}


	/*Utilisateur By ID*/
	public function getUtilisateurById($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('SELECT *, DATE_FORMAT(`dnaissance`, "%d / %m / %Y") AS dNais  FROM utilisateur WHERE id_utilisateur ='.$id_user);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}


	/*Utilisateur By email */
	public function getUtilisateurByEmail($mail){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('SELECT *, DATE_FORMAT(`dinscription`, "%Y-%m-%d %H%:i:%s") AS code_validation FROM utilisateur WHERE mail ="'.$mail.'"');
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Utilisateur By pseudo*/
	public function getUtilisateurByPseudo($pseudo){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('SELECT * FROM utilisateur WHERE pseudo="'.$pseudo.'"');
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}


	/*Insertion dans la base de données*/
	public function setUtilisateur($mail,$tel,$pseudo,$mdp){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_User = $connect->prepare("INSERT INTO `utilisateur`(`mail`, `tel`, `pseudo`, `mdp`, `dinscription`) VALUES (:mail, :tel, :pseudo, :mdp, NOW())");
		$insert_User->bindParam(':mail', $mail);
		$insert_User->bindParam(':tel', $tel);
		$insert_User->bindParam(':pseudo', $pseudo);
		$insert_User->bindParam(':mdp', $mdp);
		$insert_User->execute();
	}


	/*Modification des données USER*/
	public function UpdateUtilisateur($id_utilisateur,$nom,$prenom,$civilite,$dnaissance,$adresse,$tel,$pseudo,$description,$photo,$id_lieu,$id_situation){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `utilisateur` SET `nom`=:nom,`prenom`=:prenom,`civilite`=:civilite,`dnaissance`=:dnaissance,`adresse`=:adresse,`tel`=:tel,`pseudo`=:pseudo, `description`=:description,`photo`=:photo,`id_lieu`=:id_lieu,`id_situation`=:id_situation WHERE id_utilisateur = ".$id_utilisateur);
		$update_User->bindParam(':nom', $nom);
		$update_User->bindParam(':prenom', $prenom);
		$update_User->bindParam(':civilite', $civilite);
		$update_User->bindParam(':dnaissance', $dnaissance);
		$update_User->bindParam(':adresse', $adresse);
		$update_User->bindParam(':tel', $tel);
		$update_User->bindParam(':pseudo', $pseudo);
		$update_User->bindParam(':description', $description);
		$update_User->bindParam(':photo', $photo);
		$update_User->bindParam(':id_lieu', $id_lieu);
		$update_User->bindParam(':id_situation', $id_situation);
		$update_User->execute();
	}


	/*Modification des données USER*/
	public function UpdateEtape1Utilisateur($id_utilisateur,$id_situation){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `utilisateur` SET `id_situation`=:id_situation WHERE id_utilisateur = ".$id_utilisateur);
		$update_User->bindParam(':id_situation', $id_situation);
		$update_User->execute();
	}

	/*Modification des données USER*/
	public function UpdateValidationUtilisateur($id_utilisateur,$validation_compte){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `utilisateur` SET `validation_compte`=:validation_compte WHERE id_utilisateur = ".$id_utilisateur);
		$update_User->bindParam(':validation_compte', $validation_compte);
		$update_User->execute();
	}

	/*Modification des données USER*/
	public function UpdateEtape2Utilisateur($id_utilisateur,$nom,$prenom,$civilite,$dnaissance,$tel,$description,$id_lieu){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `utilisateur` SET `nom`=:nom,`prenom`=:prenom,`civilite`=:civilite,`dnaissance`=:dnaissance, `tel`=:tel, `description`=:description,`id_lieu`=:id_lieu WHERE id_utilisateur = ".$id_utilisateur);
		$update_User->bindParam(':nom', $nom);
		$update_User->bindParam(':prenom', $prenom);
		$update_User->bindParam(':civilite', $civilite);
		$update_User->bindParam(':dnaissance', $dnaissance);
		$update_User->bindParam(':tel', $tel);
		$update_User->bindParam(':description', $description);
		$update_User->bindParam(':id_lieu', $id_lieu);
		$update_User->execute();
	}

	/*Modification des données USER*/
	public function UpdateEtape4Utilisateur($id_utilisateur,$photo){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `utilisateur` SET `photo`=:photo WHERE id_utilisateur = ".$id_utilisateur);
		$update_User->bindParam(':photo', $photo);
		$update_User->execute();
	}

	/*Insertion dans la base de données*/
	public function setUtilisateurFB($id_utilisateur,$nom,$prenom,$mail,$photo,$pseudo,$civile){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_User = $connect->prepare("INSERT INTO `utilisateur`(`id_utilisateur`, `nom`, `prenom`, `mail`, `photo`, `pseudo`, `civilite`, `dinscription`) VALUES (:id_utilisateur, :nom, :prenom, :mail, :photo, :pseudo, :civile, NOW())");

		$insert_User->bindParam(':id_utilisateur', $id_utilisateur);
		$insert_User->bindParam(':nom', $nom);
		$insert_User->bindParam(':prenom', $prenom);
		$insert_User->bindParam(':mail', $mail);
		$insert_User->bindParam(':photo', $photo);
		$insert_User->bindParam(':pseudo', $pseudo);
		$insert_User->bindParam(':civile', $civile);
		$insert_User->execute();
	}


	/*Modification de mot de passe dans la base de données*/
	public function setUpdateMDPUtilisateur($mail,$mdp){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `utilisateur` SET `mdp` = :mdp WHERE mail = :mail");
		$update_User->bindParam(':mail', $mail);
		$update_User->bindParam(':mdp', $mdp);
		$update_User->execute();
	}

	/*Modification de mot de passe dans la base de données*/
	public function setUpdateMDP($id_utilisateur,$mdp){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `utilisateur` SET `mdp` = :mdp WHERE id_utilisateur = :id_utilisateur");
		$update_User->bindParam(':id_utilisateur', $id_utilisateur);
		$update_User->bindParam(':mdp', $mdp);
		$update_User->execute();
	}

	/*Modification de mot de passe dans la base de données*/
	public function setUpdateMail($id_utilisateur,$mail){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `utilisateur` SET `mail` = :mail WHERE id_utilisateur = :id_utilisateur");
		$update_User->bindParam(':id_utilisateur', $id_utilisateur);
		$update_User->bindParam(':mail', $mail);
		$update_User->execute();
	}

	/*Suppression de compte*/
	public function deleteUtilisateur($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('DELETE FROM  utilisateur WHERE id_utilisateur ='.$id_user);
		$req->execute(); 
	}
}
$Utilisateur = new Utilisateur();
$lesUtilisateurs = $Utilisateur->getUtilisateur();


/********** CLASSE particulier *************/
Class Particulier{

	/*Tous les particuliers*/
	public function getParticulier(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM particulier");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}


	/*Particulier By ID*/
	public function getParticulierById($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM particulier WHERE id_utilisateur =".$id_user);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Insertion dans la base de données*/
	public function setParticulier($mail,$tel,$pseudo,$mdp){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_User = $connect->prepare("INSERT INTO `particulier`(id_utilisateur, `mail`, `tel`, `pseudo`, `mdp`, `dinscription`) VALUES ((SELECT id_utilisateur FROM utilisateur ORDER BY id_utilisateur DESC LIMIT 1), :mail, :tel, :pseudo, :mdp, NOW())");
		$insert_User->bindParam(':mail', $mail);
		$insert_User->bindParam(':tel', $tel);
		$insert_User->bindParam(':pseudo', $pseudo);
		$insert_User->bindParam(':mdp', $mdp);
		$insert_User->execute();
	}

	/*Insertion dans la base de données*/
	public function setParticulierFB($id_utilisateur,$nom,$prenom,$mail,$photo,$pseudo,$civile){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_User = $connect->prepare("INSERT INTO `particulier`(`id_utilisateur`, `nom`, `prenom`, `mail`, `photo`, `pseudo`, `civilite`, `dinscription`) VALUES (:id_utilisateur, :nom, :prenom, :mail, :photo, :pseudo, :civile, NOW())");

		$insert_User->bindParam(':id_utilisateur', $id_utilisateur);
		$insert_User->bindParam(':nom', $nom);
		$insert_User->bindParam(':prenom', $prenom);
		$insert_User->bindParam(':mail', $mail);
		$insert_User->bindParam(':photo', $photo);
		$insert_User->bindParam(':pseudo', $pseudo);
		$insert_User->bindParam(':civile', $civile);
		$insert_User->execute();
	}

	/*Modification des données USER*/
	public function setParticulierUpdate($id_utilisateur, $nom, $prenom, $civilite, $dnaissance, $adresse, $mail, $tel, $pseudo, $mdp, $description, $photo, $id_situation){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_Particulier = $connect->prepare("INSERT INTO `particulier`(`id_utilisateur`, `nom`, `prenom`, `civilite`, `dnaissance`, `adresse`, `mail`, `tel`, `pseudo`, `mdp`, `description`, `photo`, `dinscription`, `id_situation`) VALUES (:id_utilisateur, :nom, :prenom, :civilite, :dnaissance, :adresse, :mail, :tel, :pseudo, :mdp, :description, :photo, NOW(), :id_situation)");
		$insert_Particulier->bindParam(':id_utilisateur', $id_utilisateur);
		$insert_Particulier->bindParam(':nom', $nom);
		$insert_Particulier->bindParam(':prenom', $prenom);
		$insert_Particulier->bindParam(':civilite', $civilite);
		$insert_Particulier->bindParam(':dnaissance', $dnaissance);
		$insert_Particulier->bindParam(':adresse', $adresse);
		$insert_Particulier->bindParam(':mail', $mail);
		$insert_Particulier->bindParam(':tel', $tel);
		$insert_Particulier->bindParam(':pseudo', $pseudo);
		$insert_Particulier->bindParam(':mdp', $mdp);
		$insert_Particulier->bindParam(':description', $description);
		$insert_Particulier->bindParam(':photo', $photo);
		$insert_Particulier->bindParam(':id_situation', $id_situation);
		$insert_Particulier->execute();
	}


	/*Modification de mot de passe dans la base de données*/
	public function setUpdateMDPParticulier($mail,$mdp){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `particulier` SET `mdp` = :mdp WHERE mail = :mail");
		$update_User->bindParam(':mail', $mail);
		$update_User->bindParam(':mdp', $mdp);
		$update_User->execute();
	}

	/*Suppression de compte particulier*/
	public function deleteParticulier($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('DELETE FROM  particulier WHERE id_utilisateur ='.$id_user);
		$req->execute(); 
	}

}
$Particulier = new Particulier();
$lesParticuliers = $Particulier->getParticulier();


/********** CLASSE professionnelle *************/
Class Professionnelle{

	/*Tous les professionnelles*/
	public function getProfessionnelle(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM professionnelle");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Professionnelle By ID*/
	public function  getProfessionnelleById($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM professionnelle WHERE id_utilisateur =".$id_user);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Insertion dans la base de données*/
	public function setProfessionnelle($mail,$tel,$pseudo,$mdp,$siret,$raison_sociale,$type_etablissement){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_User = $connect->prepare("INSERT INTO `professionnelle`(id_utilisateur, `mail`, `tel`, `pseudo`, `mdp`, `dinscription`,num_siret,raison_sociale,id_etablissement) VALUES ((SELECT id_utilisateur FROM utilisateur ORDER BY id_utilisateur DESC LIMIT 1), :mail, :tel, :pseudo, :mdp, NOW(),:siret,:raison_sociale,(SELECT id_etablissement FROM type_etablissement WHERE code_type_etablissement = :type_etablissement ))");
		$insert_User->bindParam(':mail', $mail);
		$insert_User->bindParam(':tel', $tel);
		$insert_User->bindParam(':pseudo', $pseudo);
		$insert_User->bindParam(':mdp', $mdp);
		$insert_User->bindParam(':siret', $siret);
		$insert_User->bindParam(':raison_sociale', $raison_sociale);
		$insert_User->bindParam(':type_etablissement', $type_etablissement);
		$insert_User->execute();
	}

	/*Modification des données USER*/
	public function setPprofessionnelleUpdate($id_utilisateur, $nom, $prenom, $civilite, $dnaissance, $adresse, $mail, $tel, $pseudo, $mdp, $description, $photo, $id_situation){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_Particulier = $connect->prepare("INSERT INTO `particulier`(`id_utilisateur`, `nom`, `prenom`, `civilite`, `dnaissance`, `adresse`, `mail`, `tel`, `pseudo`, `mdp`, `description`, `photo`, `dinscription`, `id_situation`) VALUES (:id_utilisateur, :nom, :prenom, :civilite, :dnaissance, :adresse, :mail, :tel, :pseudo, :mdp, :description, :photo, NOW(), :id_situation)");
		$insert_Particulier->bindParam(':id_utilisateur', $id_utilisateur);
		$insert_Particulier->bindParam(':nom', $nom);
		$insert_Particulier->bindParam(':prenom', $prenom);
		$insert_Particulier->bindParam(':civilite', $civilite);
		$insert_Particulier->bindParam(':dnaissance', $dnaissance);
		$insert_Particulier->bindParam(':adresse', $adresse);
		$insert_Particulier->bindParam(':mail', $mail);
		$insert_Particulier->bindParam(':tel', $tel);
		$insert_Particulier->bindParam(':pseudo', $pseudo);
		$insert_Particulier->bindParam(':mdp', $mdp);
		$insert_Particulier->bindParam(':description', $description);
		$insert_Particulier->bindParam(':photo', $photo);
		$insert_Particulier->bindParam(':id_situation', $id_situation);
		$insert_Particulier->execute();
	}

	/*Utilisateur By SIRET*/
	public function getProfessionnelleBySiret($siret){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('SELECT * FROM professionnelle WHERE num_siret="'.$siret.'"');
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Modification de mot de passe dans la base de données*/
	public function setUpdateMDPProfessionnelle($mail,$mdp){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$update_User = $connect->prepare("UPDATE `professionnelle` SET `mdp` = :mdp WHERE mail = :mail");
		$update_User->bindParam(':mail', $mail);
		$update_User->bindParam(':mdp', $mdp);
		$update_User->execute();
	}

	/*Suppression de compte professionnelle*/
	public function deleteProfessionnelle($id_user){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('DELETE FROM  professionnelle WHERE id_utilisateur ='.$id_user);
		$req->execute(); 
	}

}
$Professionnelle = new Professionnelle();
$lesProfessionnelles = $Professionnelle->getProfessionnelle();


/********** CLASSE Type_service *************/
Class TypeService{
	/*Afficher tous les types d'annonces*/
	public function getTypeService(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('SELECT * FROM type_service');
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Afficher tous les types d'annonces*/
	public function getTypeServiceById($id_type_service){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare('SELECT * FROM type_service WHERE id_type_service = '.$id_type_service);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}
}
$TypeService = new TypeService();
$lesTypeServices = $TypeService->getTypeService();