<?php 
    require_once "../config/fonctions.php";
    /*Sessions*/
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }
    }

    $userProfil = $Utilisateur->getUtilisateurById(1);
?>
<!DOCTYPE html>
<html  >
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Profil de <?php echo $userProfil['nom']." ".$userProfil['prenom']; ?></title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style>          
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" once="menu" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../deposer_annonce/"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="engine">
            <a href="#">website maker</a>
        </section>

        <section class="features1 cid-rVmE73Fmjs" id="depot_annonce">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-5">
                        <div class="form-container">
                            <div class="media-container-column" data-form-type="formoid">
                                
                                <!-- Mon annonce -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-8 mb-5">
                                        
                                            <div class="card bg-light" >
                                                <div class="card-body border text-left">
                                                    <span class="float-right">
                                                        <a href="index.php?edit" class="display-7 border p-2"><span class="mbri-user"></span> Modifier mon profil</a>
                                                    </span>&nbsp;
                                                    <h1 class="card-title"><?php echo $userProfil['pseudo']; ?></h1>

                                                    <!-- Photo -->
                                                    <div class="card border mt-4 p-2 h-75">
                                                        <div class="row">
                                                            <div class="col-sm-4 col-md-4">
                                                                <a href="#"><img  src="https://saccade.ca/img/autiste-apropos.svg" alt="..." class="rounded-circle img-fluid border w-100"></a>
                                                            </div>
                                                            <div class="col-sm-8 col-md-8">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        Avis :
                                                                        <?php 
                                                                            
                                                                            avisUser(1); 
                                                                        ?>
                                                                    </li>
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        Statut :
                                                                        <span><strong>Particulier</strong></span>
                                                                    </li>
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        Date d'inscription :
                                                                        <span><strong>14/14/2020</strong></span>
                                                                    </li>
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        Âge :
                                                                        <span><strong>33 ans</strong></span>
                                                                    </li>
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        Je suis :
                                                                        <span><strong>une femme</strong></span>
                                                                    </li>
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        Situation :
                                                                        <span><strong></strong></span>
                                                                    </li>
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        Connecté :
                                                                        <span><strong>- de 15 jours</strong></span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Button contacter -->
                                                    <div class="mt-2 mb-4">
                                                        <a href="#" class="card-link btn border p-3"><span class="mbri-letter mbr-iconfont mbr-iconfont-btn"></span> Envoyer un message</a>
                                                        <a href="#" class="card-link btn border p-3"><span class="mbri-mobile mbr-iconfont mbr-iconfont-btn"></span> Voir le numéro</a>
                                                    </div>
                                                        
                                                    <!-- Son annonce (1) -->
                                                    <div class="mb-4">
                                                        <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">Son annonce (1)</h2>
                                                        <div class="card-body border mt-2">
                                                            <a href="#">
                                                                <div class="row">
                                                                    <div class="col-md-4 col-6 col-sm-4">
                                                                        <small class="text-muted">Aujourd'hui à 08:30</small>
                                                                    </div>
                                                                    <div class="col-md-4 col-6 col-sm-4 ml-auto boder">
                                                                        <button type="button">20€/h</button>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-3 col-sm-3">
                                                                        <img src="https://www.mahoraisedeseaux.com/bundles/eticsitesogea/images/logo.jpg" class="img-fluid" alt="Nom du site">
                                                                    </div>
                                                                    <div class="col-md-9 col-9 col-sm-9">
                                                                        <strong>Titre de l'annonce</strong>
                                                                        <br>
                                                                        <p class="text-muted" style="text-align: left;">
                                                                            Description : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="d-flex justify-content-around">
                                                                  <small class="p-0"><span class="mbri-pin"></span> Lieu</small>
                                                                  <small class="p-0"><span class="mbri-sale"></span> Catégorie</small>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <!-- Avis -->
                                                    <div class="mb-4">
                                                        <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">Ses avis</h2>
                                                        <p class="text-left">
                                                            2 avis : <strong class="text-success">note moyenne 5/5</strong>
                                                        </p>
                                                        <!-- Les avis -->
                                                        <div class="bg-light">
                                                            <div class="card-body border mt-2">
                                                                <a href="#">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-6 col-sm-4">
                                                                            <small class="text-muted">Aujourd'hui à 08:30</small>
                                                                        </div>
                                                                        <div class="col-md-4 col-6 col-sm-4 ml-auto boder">
                                                                            <button type="button">
                                                                                <span class="text-success">
                                                                                    <span class="mbri-star"></span>
                                                                                    <span class="mbri-star"></span>
                                                                                    <span class="mbri-star"></span>
                                                                                    <span class="mbri-star"></span>
                                                                                    <span class="mbri-star"></span>
                                                                                </span>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-3 col-sm-3">
                                                                            <img src="https://saccade.ca/img/autiste-apropos.svg" class="img-fluid" alt="Nom du site">
                                                                        </div>
                                                                        <div class="col-md-9 col-9 col-sm-9">
                                                                            <strong>Toto TOTO</strong>
                                                                            <br>
                                                                            <strong>Titre de l'annonce</strong>
                                                                            <br>
                                                                            <p class="text-muted" style="text-align: left;">
                                                                                Merci beaucoup à Nina et Mel d'avoir gardé Drogo quelques jours, il s'est beaucoup amusé et les filles sont d'un très grand professionnalisme. Elle donne beaucoup d'informations et répondent à toutes nos questions.
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="card-body border mt-2">
                                                                <a href="#">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-6 col-sm-4">
                                                                            <small class="text-muted">Aujourd'hui à 08:30</small>
                                                                        </div>
                                                                        <div class="col-md-4 col-6 col-sm-4 ml-auto boder">
                                                                            <button type="button">
                                                                                <span class="text-success">
                                                                                    <span class="mbri-star"></span>
                                                                                    <span class="mbri-star"></span>
                                                                                    <span class="mbri-star"></span>
                                                                                    <span class="mbri-star"></span>
                                                                                    <span class="mbri-star"></span>
                                                                                </span>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-3 col-sm-3">
                                                                            <img src="https://saccade.ca/img/autiste-apropos.svg" class="img-fluid" alt="Nom du site">
                                                                        </div>
                                                                        <div class="col-md-9 col-9 col-sm-9">
                                                                            <strong>Tata TATA</strong>
                                                                            <br>
                                                                            <strong>Titre de l'annonce</strong>
                                                                            <br>
                                                                            <p class="text-muted" style="text-align: left;">
                                                                                Deux jeunes femmes adorables et très disponibles pour la garde pendant les vacances d'été de ma petite boule de poil.
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!--  -->
                                                        <p class="text-center mt-4">
                                                            <button class="border bg-light"><span class="mbri-like"></span> Ajouter un avis</button>
                                                        </p>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Deuxième colonne -->
                                        <div class="col-md-4">

                                        </div>
                                    </div> 
                                </div>
                                
                            </div>
                        </div>
                                
                    </div>
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../assets/parallax/jarallax.min.js"></script>
        <script src="../assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../assets/tether/tether.min.js"></script>
        <script src="../assets/theme/js/script.js"></script>
        <script src="../assets/formoid/formoid.min.js"></script>
        <script type="text/javascript" src="../assets/monjs/disponibilite.js"></script>
    </body>
</html>
