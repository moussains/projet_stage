<?php 
    require_once "../config/fonctions.php";
    /*Sessions*/
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }else{
            header("Location: ../");
        }
    }

	//Si étape 1 on modifie la situation
	if (isset($_POST['etape1'])) {
		$Utilisateur->UpdateEtape1Utilisateur($user['id_utilisateur'], $_POST['situation']);
	}

	//Si étape 2
	if (isset($_POST['etape2'])) {
		$nom = $_POST['nom'];
		$prenom = $_POST['prenom'];
		$civilite = $_POST['civilite'];
		$date_naissance = $_POST['date_naissance'];
		$tel = $_POST['tel'];
		$ville = $_POST['ville'];
		$description = $_POST['description'];

		$Utilisateur->UpdateEtape2Utilisateur($user['id_utilisateur'],$nom,$prenom,$civilite,$date_naissance,$tel,$description,$ville);
	}

	//Si étape 3
	if (isset($_POST['etape3'])) {
		updateCompetencesInscritpion($user['id_utilisateur'], $_POST['competence']);
	}

	//Si étape 4
	if (isset($_POST['etape4'])) {
		# code...
	}
