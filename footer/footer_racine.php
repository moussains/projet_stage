<?php
    require_once "config/classes.php";
?>
<section class="cid-rVmzLBNU5T" id="footer1-6">
    <div class="container">
        <div class="row  text-black">
            <!-- Le logo -->
            <div class="col-md-3 col-sm-12 col-12 align-center">
                <div class="media-wrap">
                    <a href="#/"><img src="assets/images/logo2.png" alt="Nom du site"></a>
                </div>
            </div>
            <!-- Site -->
            <div class="col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-2 pt-3 ">
                    SITE :
                </h5>
                <p class="mbr-text">
                    <a class="text-primary" href="#">Qui sommes-nous ?</a>
                    <br>
                    <a class="text-primary" href="#">Mentions légales</a>
                    <br>
                    <a class="text-primary" href="#">Contactez nous</a>
                </p>
            </div>
            <!-- Services -->
            <div class="col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-2 pt-3">
                    SERVICES :
                </h5>
                <p class="mbr-text">
                    <a class="text-primary" href="#">Aide</a>
                </p>
            </div>
            <!-- Top catégorie -->
            <div class="col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-2 pt-3">
                    TOP CATÉGORIES :
                </h5>
                <p class="mbr-text">
                    <!-- On parcourt les categories de 1 à 5 -->
                    <?php foreach ($Categorie->getLimitCategories(0,5) as $categorie): ?>
                        <a class="text-primary" href="#<?php echo $categorie['id_cat']; ?>"><?php echo $categorie['lib_cat']; ?></a>
                        <br>
                    <?php endforeach ?>
                </p>
            </div>
            <!-- Vide -->
            <div class="col-md-3 mbr-fonts-style display-7">
            </div>
            <!-- Annonces -->
            <div class="col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-2 pt-3">
                    ANNONCES :
                </h5>
                <p class="mbr-text">
                    <a class="text-primary" href="#">Déposer une annonce</a>
                    <br>
                    <a class="text-primary" href="#">Visualiser les annonces</a>
                </p>
            </div>
            <!-- Mots clés -->
            <div class="col-md-6 mbr-fonts-style display-7">
                <h5 class="pb-2 pt-3">
                    MOTS CLÉS :
                </h5>
                <p class="mbr-text">
                    <a class="text-primary" href="#">Tant</a> - 
                    <a class="text-primary" href="#">Trop</a> - 
                    <a class="text-primary" href="#">Néanmoins</a> - 
                    <a class="text-primary" href="#">Par-dessous</a> - 
                    <a class="text-primary" href="#">Également</a> - 
                    <a class="text-primary" href="#">Comment</a> - 
                    <a class="text-primary" href="#">Autrement</a> - 
                    <a class="text-primary" href="#">Maintenant</a> - 
                    <a class="text-primary" href="#">Longtemps</a>
                    
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright text-black">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020 Nom du site - Tous droits réservés
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="#" target="_blank"><span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span></a>
                        </div>
                        <div class="soc-item">
                            <a href="#" target="_blank"><span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>