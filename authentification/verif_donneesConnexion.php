<?php
	/*Les tables (requetes)*/
    require_once "../config/classes.php";
    /*les variables*/
	$message = "";
	$user_name = "";
	$erreur = 0;

	if (isset($_GET['lemail']) && isset($_GET['lemdp'])) {

		$lemail = $_GET['lemail'];
		$lemdp = md5($_GET['lemdp']);

		$user = $Utilisateur->getUtilisateurByEmail($lemail);

		$mail = $lemail;

		if (isset($user['mail']) && $user['mail'] == $lemail) {
			if ($user['mdp'] == $lemdp) {

				$message .= 'Vous êtes connecté !';
				$user_name = $user['pseudo'];
				session_start();
				$_SESSION['id_utilisateur'] = $user['id_utilisateur'];
			}else{
				$message .= 'Les identifiants entrés sont incorrects.';
				$erreur = 1;
			}
		}else{
			$message .= "Les identifiants entrés sont incorrects.";
			$erreur = 2;
		}
	}
	echo '{"message":"'.$message.'", "erreur":"'.$erreur.'", "user_name":"'.$user_name.'"}';

	