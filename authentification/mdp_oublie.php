<?php 
	require_once "../config/fonctions.php";

	$retour = "";

	if(isset($_GET['lemail'])){
		if (empty($Utilisateur->getUtilisateurByEmail($_GET['lemail'])['id_utilisateur'])) {
			$retour = "ok";
		}else{
			mailReintialisationMDP($_GET['lemail']);
			$retour = $Utilisateur->getUtilisateurByEmail($_GET['lemail'])['mail'];
		}
		
	}

	echo '{"resultat":"'.$retour.'"}';