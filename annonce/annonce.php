<?php 
    require_once "../config/fonctions.php";
    /*Sessions*/
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }
    }

    /*Les variables */
    $lannonce="";
    $sigleTarifAnnonce = "";
    $userAnnonce = "";
    $lieuAnnonce = "";
    $sousCatAnnonce = [];
    $dateFormatAnnonce = "";
    $planifier = "";

    /*Vérification si une annonce*/
    if (isset($_GET['id_annonce'])) {
        /*L'annonce */
       $lannonce = $Annonce->getAnnonceById($_GET['id_annonce']);
        /*le sigle de la table type de tarification*/
        $sigleTarifAnnonce = $TypeTarification->getTypeTarificationById($lannonce['id_type_tarif'])['sigle_type_tarif'];
        /*les infos de l'utilisateur qui publie de la table utilisateur*/
        $userAnnonce = $Utilisateur->getUtilisateurById($lannonce['id_utilisateur']);
        /*le lieu de la table Lieu*/
        $lieuAnnonce = $Lieu->getLieuById($lannonce['id_lieu']);
        /*Scatégorie pour une annonce*/
        $CatAnnonce = $Categorie->getCategorieByAnnonce($lannonce['id_annonce']);
        /*Sous catégorie pour une annonce*/
        $sousCatAnnonce = $SousCategorie->getSousCategorieByAnnonce($lannonce['id_annonce']);
        /*Le format de la date de publication annonce*/
        $dateFormatAnnonce = dateFormatAnnoncePub($lannonce['id_annonce']);
        /*Les qautres dernières annonces de la table annonce*/
        $lesQuatresDernieresAnnonces = $Annonce->getAnnonceByQuatreDernieres($lannonce['id_annonce'], $sousCatAnnonce['id_sous_cat']);
        



    }else{
        header("Location: ./");
    }


    /*Les heures de la table planifier pour l'annonce*/
    $nbDispo = 0;

    $d_lundi = "";
    $d_mardi = "";
    $d_mercredi = "";
    $d_jeudi = "";
    $d_vendredi = "";
    $d_samedi = "";
    $d_dimanche = "";
    $f_lundi = "";
    $f_mardi = "";
    $f_mercredi = "";
    $f_jeudi = "";
    $f_vendredi = "";
    $f_samedi = "";
    $f_dimanche = "";

    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(1, $lannonce['id_annonce'])['h_debut'])){
        $d_lundi = $Planifier->getPlanifierByIdJourAndAnnonce(1, $lannonce['id_annonce'])['h_debut'];
        $nbDispo = $nbDispo + 1;
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(2, $lannonce['id_annonce'])['h_debut'])){
        $d_mardi = $Planifier->getPlanifierByIdJourAndAnnonce(2, $lannonce['id_annonce'])['h_debut'];
        $nbDispo = $nbDispo + 1;
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(3, $lannonce['id_annonce'])['h_debut'])){
        $d_mercredi = $Planifier->getPlanifierByIdJourAndAnnonce(3, $lannonce['id_annonce'])['h_debut'];
        $nbDispo = $nbDispo + 1;
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(4, $lannonce['id_annonce'])['h_debut'])){
        $d_jeudi = $Planifier->getPlanifierByIdJourAndAnnonce(4, $lannonce['id_annonce'])['h_debut'];
        $nbDispo = $nbDispo + 1;
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(5, $lannonce['id_annonce'])['h_debut'])){
        $d_vendredi = $Planifier->getPlanifierByIdJourAndAnnonce(5, $lannonce['id_annonce'])['h_debut'];
        $nbDispo = $nbDispo + 1;
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(6, $lannonce['id_annonce'])['h_debut'])){
        $d_samedi = $Planifier->getPlanifierByIdJourAndAnnonce(6, $lannonce['id_annonce'])['h_debut'];
        $nbDispo = $nbDispo + 1;
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(7, $lannonce['id_annonce'])['h_debut'])){
        $d_dimanche = $Planifier->getPlanifierByIdJourAndAnnonce(7, $lannonce['id_annonce'])['h_debut'];
        $nbDispo = $nbDispo + 1;
    }
    

    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(1, $lannonce['id_annonce'])['h_fin'])){
        $f_lundi = $Planifier->getPlanifierByIdJourAndAnnonce(1, $lannonce['id_annonce'])['h_fin'];
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(2, $lannonce['id_annonce'])['h_fin'])){
        $f_mardi = $Planifier->getPlanifierByIdJourAndAnnonce(2, $lannonce['id_annonce'])['h_fin'];
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(3, $lannonce['id_annonce'])['h_fin'])){
        $f_mercredi = $Planifier->getPlanifierByIdJourAndAnnonce(3, $lannonce['id_annonce'])['h_fin'];
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(4, $lannonce['id_annonce'])['h_fin'])){
        $f_jeudi = $Planifier->getPlanifierByIdJourAndAnnonce(4, $lannonce['id_annonce'])['h_fin'];
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(5, $lannonce['id_annonce'])['h_fin'])){
        $f_vendredi = $Planifier->getPlanifierByIdJourAndAnnonce(5, $lannonce['id_annonce'])['h_fin'];
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(6, $lannonce['id_annonce'])['h_fin'])){
        $f_samedi = $Planifier->getPlanifierByIdJourAndAnnonce(6, $lannonce['id_annonce'])['h_fin'];
    }
    
    if (!empty($Planifier->getPlanifierByIdJourAndAnnonce(7, $lannonce['id_annonce'])['h_fin'])){
        $f_dimanche = $Planifier->getPlanifierByIdJourAndAnnonce(7, $lannonce['id_annonce'])['h_fin'];
    }
    
    /*Type d'annonce */
    $typeAnnonce = $TypeService->getTypeServiceById($lannonce['id_type_service']);
?> 

<!DOCTYPE html>
<html  lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../../../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Annonce</title>
        <link rel="stylesheet" href="../../../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../../../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../../../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../../../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../../../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../../../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../../../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../../../assets/mobirise/css/mbr-additional.css" type="text/css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style> 
            @media screen and (max-width: 767.98px) {
                .bouton_proposer{
                    position: fixed;
                    z-index: 100;
                    bottom: 0;
                    width: 85%;
                }
            }         
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../../../"><img src="../../../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../../../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-black display-4" href="../../../#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../../../deposer_annonce/"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../../../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../../../authentification/sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../../../authentification/login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../../../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>

        <section class="features1 cid-rVmE73Fmjs" id="depot_annonce">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-5">
                        <div class="form-container">
                            <div class="media-container-column" data-form-type="formoid">
                                
                                <!-- Mon annonce -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-8 mb-5">
                                        
                                            <div class="card bg-light" >
                                                <div class="card-body border text-left">
                                                    <h1 class="card-title"><?php echo $lannonce['lib_annonce']." - ".$lieuAnnonce['nom_lieu']; ?></h1>

                                                    <!-- Photo -->
                                                    <!-- <div class="card border" style="height: 188px;">
                                                        
                                                    </div> -->

                                                    <!-- Button contacter -->
                                                    <!-- <div class="mt-2 mb-4">
                                                        <a href="#" class="card-link btn border p-3"><span class="mbri-letter mbr-iconfont mbr-iconfont-btn"></span> Envoyer un message</a>
                                                        <a href="#" class="card-link btn border p-3"><span class="mbri-mobile mbr-iconfont mbr-iconfont-btn"></span> Voir le numéro</a>
                                                    </div> -->
                                                        
                                                    <!-- Description -->
                                                    <div class="mb-4">
                                                        <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">Description</h2>
                                                        <p class="card-text text-left">
                                                            <?php echo nl2br($lannonce['description']); ?>
                                                        </p>
                                                    </div>

                                                    

                                                    <!-- Disponibilité -->
                                                    <!-- <div class="mb-4">
                                                        <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">Disponibilité (<?php echo $nbDispo; ?>j / 7)</h2>
                                                        <div class="table-responsive mb-2">
                                                            <table class="table table-striped table-bordered text-center">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">Lundi</th>
                                                                        <th scope="col">Mardi</th>
                                                                        <th scope="col">Mercredi</th>
                                                                        <th scope="col">Jeudi</th>
                                                                        <th scope="col">Vendredi</th>
                                                                        <th scope="col">Samedi</th>
                                                                        <th scope="col">Dimanche</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>

                                                                    <tr>
                                                                        <td>
                                                                            <?php echo $d_lundi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $d_mardi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $d_mercredi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $d_jeudi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $d_vendredi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $d_samedi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $d_dimanche; ?>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <?php echo $f_lundi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $f_mardi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $f_mercredi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $f_jeudi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $f_vendredi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $f_samedi; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $f_dimanche; ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <p class="card-subtitle text-muted text-left"> 
                                                            <?php if (isset($lannonce['dtedebut'])): ?>
                                                                <span class="mbri-calendar mbr-iconfont mbr-iconfont-btn"></span> Du <?php echo $lannonce['dtedebut']; ?> au <?php echo $lannonce['dtefin']; ?>
                                                            <?php endif ?>
                                                        </p>
                                                    </div> -->

                                                    <!-- Avis -->
                                                    <div class="mb-4">
                                                        <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">Avis(0)</h2>
                                                        <p class="text-center">
                                                            <small>Soyez le premier à déposer un avis sur cette annonce.</small> <button class="border bg-light">Ajouter un avis</button>
                                                        </p>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Deuxième colonne -->
                                        <div class="col-md-4">
                                            <div class="card bg-light mb-2" >
                                                <div class="card-body border align-center">
                                                    <span >
                                                        <img  src="https://www.mahoraisedeseaux.com/bundles/eticsitesogea/images/logo.jpg" alt="..." class="rounded-circle img-fluid border w-25 ">
                                                    </span>
                                                    
                                                    <h3><a href="#"><?php echo $userAnnonce['pseudo']; ?></a></h3>
                                                </div>
                                                <div class="card-footer border">
                                                    <p class="text-left">Type d'annonce : <strong><?php echo $typeAnnonce['lib_type_service']; ?></strong></p>
                                                    <p class="text-left">Catégorie : <strong><?php echo $CatAnnonce['lib_cat']." - ".$sousCatAnnonce['lib_sous_cat']; ?></strong></p>
                                                    <p class="text-left">Localisation : <strong><?php echo $lieuAnnonce['nom_lieu']." (".$lieuAnnonce['cp'].")" ; ?></strong></p>
                                                    <p class="text-left">Prix : 
                                                        <strong>
                                                            <?php echo $lannonce['prix']."€ "; ?>
                                                            <!-- si Une annonce est gratuit ou sur devis, on affiche que le type de tarification -->
                                                            <!-- <?php if ($lannonce['prix'] == 0 && $lannonce['id_type_tarif'] == 4 || $lannonce['id_type_tarif'] == 5): ?>
                                                                <?php if ($lannonce['id_type_tarif'] == 4 ): ?>
                                                                    <?php echo $sigleTarifAnnonce; ?>
                                                                <?php endif ?>
                                                                <?php if ($lannonce['id_type_tarif'] == 5 ): ?>
                                                                    <?php echo $sigleTarifAnnonce; ?>
                                                                <?php endif ?>
                                                            <?php else: ?>
                                                                <?php echo $lannonce['prix']."€ / ".$sigleTarifAnnonce; ?>
                                                            <?php endif ?> -->
                                                        </strong>
                                                    </p>
                                                    <p class="text-left">Publiée : <strong><?php echo $dateFormatAnnonce; ?></strong></p>
                                                </div>
                                            </div>

                                            <div class="card bg-light mb-2 p-0 bouton_proposer" id="btn_proposer">
                                                <!-- Button trigger modal -->
                                                <button class="btn btn-info border m-0" data-toggle="modal" data-target="#exampleModalCenter" id="bouton_p">
                                                    <p class="p-0 m-0">Proposer mes services</p>
                                                </button>
                                            </div>

                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content h-25">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">
                                                                Proposer mes services
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                             <div class="card bg-light my-4 p-0 " id="">
                                                                <!-- Button trigger modal -->
                                                                <!-- <button class="btn btn-info border m-0" id="affiche_tel">
                                                                    <i class="fa fa-phone mr-2"></i> Afficher le N° de téléphone
                                                                </button> -->
                                                                <span class="h2 text-center text-secondary pt-4" id="bloc_tel">
                                                                    <?php 
                                                                        /*numéro de tel avec des espaces*/
                                                                        $num = $lannonce['tel_annonce'];
                                                                        $numero = str_pad(substr($num, 0, 2),3).str_pad(substr($num, 2,2), 3, " ", STR_PAD_BOTH).str_pad(substr($num, 4,2), 3, " ", STR_PAD_BOTH).str_pad(substr($num, 6,2), 3, " ", STR_PAD_BOTH).str_pad(substr($num, 8,2), 3, " ", STR_PAD_BOTH);

                                                                        echo "Tél : <a href=\"tel:".$lannonce['tel_annonce']."\">".$numero."</a>";
                                                                    ?>    
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card bg-light mb-2" >
                                                <div class="card-head border text-center p-2">
                                                    <p class="p-0 m-0">Les dernières annonces de la même catégorie</p>
                                                </div>
                                                <div class="card-body border text-left align-left">
                                                    <?php foreach ($lesQuatresDernieresAnnonces as $annonce): ?>
                                                        <?php 
                                                            $lieuAnnon = $Lieu->getLieuById($annonce['id_lieu']); 
                                                            /*le sigle de la table type de tarification*/
                                                            $TarifAnnonce = $TypeTarification->getTypeTarificationById($annonce['id_type_tarif']);
                                                            /*Sous catégorie pour une annonce*/
                                                            $sousCatAnnonce = $SousCategorie->getSousCategorieByAnnonce($annonce['id_annonce'])['lib_sous_cat'];
                                                        ?>
                                                        <div class="border-bottom py-2 ">
                                                            <div class="row">
                                                                <div class=" col-4 col-sm-4 col-md-4">
                                                                    <a href="../../<?php echo enleverCaracteresSpeciaux($sousCatAnnonce).'/'.enleverCaracteresSpeciaux($annonce['lib_annonce']).'-'.$annonce['id_annonce'].'/'; ?>"><img  src="https://www.mahoraisedeseaux.com/bundles/eticsitesogea/images/logo.jpg" alt="..." class="rounded-circle img-fluid border w-75"></a>
                                                                </div>
                                                                <div class="col-8 col-sm-8 col-md-8">
                                                                    <a href="../../<?php echo enleverCaracteresSpeciaux($sousCatAnnonce).'/'.enleverCaracteresSpeciaux($annonce['lib_annonce']).'-'.$annonce['id_annonce'].'/'; ?>"><h5><?php echo $annonce['lib_annonce']; ?></h5>
                                                                    </a>
                                                                    <p class="text-left m-0">

                                                                        <!-- si Une annonce est gratuit ou sur devis, on affiche que le type de tarification -->
                                                                        <?php  echo "<strong>".$annonce['prix']." €</strong>"; ?>

                                                                    </p>
                                                                    <small class="text-muted">
                                                                        <span class="mbri-pin"></span> <?php echo $lieuAnnon['nom_lieu']; ?>
                                                                    </small>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                
                            </div>
                        </div>
                                
                    </div>
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine_annonce.php";
        ?>
        <!-- Fin section footer -->
        <script src="../../../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../../../assets/popper/popper.min.js"></script>
        <script src="../../../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../../../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../../../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../../../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../../../assets/parallax/jarallax.min.js"></script>
        <script src="../../../assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="../../../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../../../assets/tether/tether.min.js"></script>
        <script src="../../../assets/theme/js/script.js"></script>
        <script src="../../../assets/formoid/formoid.min.js"></script>
        <script type="text/javascript" src="../../../assets/monjs/disponibilite.js"></script>
        <script type="text/javascript">
            // Si Fait glisser la fenêtre vers une position particulière du document
            window.onscroll = function() {gestionBtnScroll()};

            var btnproposer = document.getElementById("btn_proposer");
            var posBtnproposer = btnproposer.offsetTop + btnproposer.offsetHeight;

            function gestionBtnScroll() {
                /*Position du bouton*/
                if (window.pageYOffset >= posBtnproposer) {
                    btnproposer.classList.remove("bouton_proposer");
                }else{
                    btnproposer.classList.add("bouton_proposer")
                }
            }
        </script>
        <script type="text/javascript">
            $(function(){
                var bouton_p = $('#bouton_p'), affiche_tel = $('#affiche_tel'), bloc_tel = $('#bloc_tel');

                /*bouton_p.click(function(){
                    bloc_tel.hide();
                })

                affiche_tel.click(function(){
                    bloc_tel.show();
                })*/
                
            })
            
        </script>
    </body>
</html>
