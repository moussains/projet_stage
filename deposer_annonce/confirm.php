<?php 
    require_once "../config/fonctions.php";
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }else{
            header("Location: ../");
        }
    }
    if (isset($_POST['deposer'])) {

        /*Insertion dans la table annonce et table annon_cat_souscat*/
        depotAnnonce($_POST['lib_annon'], $_POST['description'], $_POST['adresse'], $_POST['dte_debut'], $_POST['dte_fin'], $_POST['prix'], $_POST['id_type_tarif'], $_POST['id_cat'], $_POST['id_sous_cat'], $_POST['id_user'], $_POST['id_user_reserver'], $_POST['id_lieu'], $_POST['type_service'], $_POST['tel']);


        /*On crée le tableau pour récupérer toutes les heures des jours*/
        $lesjours = array(
                        array('name' => $_POST['d_lundi']),
                        array('name' => $_POST['d_mardi']),
                        array('name' => $_POST['d_mercredi']),
                        array('name' => $_POST['d_jeudi']),
                        array('name' => $_POST['d_vendredi']),
                        array('name' => $_POST['d_samedi']),
                        array('name' => $_POST['d_dimanche']),
                        array('name' => $_POST['f_lundi']),
                        array('name' => $_POST['f_mardi']),
                        array('name' => $_POST['f_mercredi']),
                        array('name' => $_POST['f_jeudi']),
                        array('name' => $_POST['f_vendredi']),
                        array('name' => $_POST['f_samedi']),
                        array('name' => $_POST['f_dimanche'])
                    );

        $dernierAnnon = $Annonce->getDerniereAnnonce();

        /*on parcourt le tableau en vérifiant s'il y a des jours qui ne sont pas null*/
        for ($i = 0, $size = count($lesjours); $i < $size; ++$i) { 
            /*Insertion dans la table planifier si ce n'est pas  null*/
            if (!empty($lesjours[$i]['name'])) {
                /*Lundi*/
                if ($i == 0 ) {
                    $Planifier->setPlanifier($dernierAnnon['id_annonce'], 1, $lesjours[$i]['name'], $lesjours[7]['name']);
                }
                /*Mardi*/
                if ($i == 1) {
                    $Planifier->setPlanifier($dernierAnnon['id_annonce'], 2, $lesjours[$i]['name'], $lesjours[8]['name']);
                }
                /*Mercredi*/
                if ($i == 2) {
                    $Planifier->setPlanifier($dernierAnnon['id_annonce'], 3, $lesjours[$i]['name'], $lesjours[9]['name']);
                }
                /*Jeudi*/
                if ($i == 3) {
                    $Planifier->setPlanifier($dernierAnnon['id_annonce'], 4, $lesjours[$i]['name'], $lesjours[10]['name']);
                }
                /*Vendredi*/
                if ($i == 4) {
                    $Planifier->setPlanifier($dernierAnnon['id_annonce'], 5, $lesjours[$i]['name'], $lesjours[11]['name']);
                }
                /*Samedi*/
                if ($i == 5) {
                    $Planifier->setPlanifier($dernierAnnon['id_annonce'], 6, $lesjours[$i]['name'], $lesjours[12]['name']);
                }
                /*Dimanche*/
                if ($i == 6) {
                    $Planifier->setPlanifier($dernierAnnon['id_annonce'], 7, $lesjours[$i]['name'], $lesjours[13]['name']);
                }
            }
        }    
    }else{
        header('Location: ./')
    }
?>
<!DOCTYPE html>
<html  lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Confirmation</title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <style>
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-black display-4" href="./"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>

        <section class="features1 cid-rVmE73Fmjs" id="depot_annonce">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-5">
                        <div class="form-container">
                            <div class="media-container-column" data-form-type="formoid">
                                
                                <!-- Mon annonce -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="card w-100" >
                                            <div class="card-body border text-center">
                                                <h4 style="color : green;" class=" mb-5 card-title">Votre dépôt d'annonce a bien été pris en compte :-) !</h4>
                                                <h5 class="card-subtitle mb-2 text-muted">Votre annonce est en cours de validation ... </h5>

                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                   
                                    
                                <!---Formbuilder Form--->
                                <div class="col-md-12 ">
                                    <form action="#" method="POST" class="mbr-form form-with-styler mt-3" >
                                        <!-- Button Déposer -->
                                        <div class="row align-center">
                                            <div class="col-md-6 offset-md-3 input-group-btn">
                                                <a href="../" class="btn btn-form btn-info display-4">Ok</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                
                    </div>
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../assets/parallax/jarallax.min.js"></script>
        <script src="../assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../assets/tether/tether.min.js"></script>
        <script src="../assets/theme/js/script.js"></script>
        <script src="../assets/formoid/formoid.min.js"></script>
        <script type="text/javascript" src="../assets/monjs/disponibilite.js"></script>
    </body>
</html>
