<?php
    require_once "../config/fonctions.php";
    $connexion = false;
    $user = [];
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['id_utilisateur'])) {
            /*les infos de l'utilisateur qui publie de la table utilisateur*/
            $user = $Utilisateur->getUtilisateurById($_SESSION['id_utilisateur']);
            $connexion = true;
        }else{
            header("Location: ../");
        }
    }



    $ville = "";
    // $adresse = "";
    $tel = "";
    $cat = "";
    $sous_cat = "";
    $titre_annonce = "";
    $description = "";
    $type_tarif = "5";
    $type_service = "";
    $prix = "0";
    $periode = "";
    $j_semaine = "";
    $debut_periode = "";
    $fin_periode = "";
    $id_jour_lundi = "";
    $id_jour_mardi = "";
    $id_jour_mercredi = "";
    $id_jour_jeudi = "";
    $id_jour_vendredi = "";
    $id_jour_samedi = "";
    $id_jour_dimanche = "";
    $h_debut_lundi = "";
    $h_debut_mardi = "";
    $h_debut_mercredi = "";
    $h_debut_jeudi = "";
    $h_debut_vendredi = "";
    $h_debut_samedi = "";
    $h_debut_dimanche = "";
    $h_fin_lundi = "";
    $h_fin_mardi = "";
    $h_fin_mercredi = "";
    $h_fin_jeudi = "";
    $h_fin_vendredi = "";
    $h_fin_samedi = "";
    $h_fin_dimanche = "";


    $nbdispo = 0;
    

    /*Vérification si on a cliqué sur prévisualiser*/
    if (isset($_POST['previsualiser'])) {


        $ville = $_POST['ville'];
        //$adresse = $_POST['adresse'];
        $tel = $_POST['tel'];
        $cat = $_POST['categorie'];
        $sous_cat = $_POST['souscategorie'];
        $titre_annonce = $_POST['titre_annonce'];
        $description = $_POST['description'];
        $type_service = $_POST['type_annonce'];
        if (isset($_POST['type_tarif'])) {
            $type_tarif = $_POST['type_tarif'];
        }
        

        /*Vérification pour le prix*/
        if (isset($_POST['prix'])) {
            $prix = $_POST['prix'];
        }

        /*Vérification pour checkbox période*/
        if (isset($_POST['periode'])) {
            $periode = $_POST['periode'];
            $debut_periode = dateFormatAnglais($_POST['debut_periode']);
            $fin_periode = dateFormatAnglais($_POST['fin_periode']);

        }

        /*Vérification pour checkbox j_semaine*/
        if (isset($_POST['jour_semaine'])) {
            $j_semaine = $_POST['jour_semaine'];

        }
        

        /*Vérification pour les checkbox des Jours de la semaine*/
        if(isset($_POST['id_jour_lundi'])){
            $id_jour_lundi = $_POST['id_jour_lundi'];
            $h_debut_lundi = $_POST['h_debut_lundi'];
            $h_fin_lundi = $_POST['h_fin_lundi'];
            $nbdispo = $nbdispo+1;
        }
        if(isset($_POST['id_jour_mardi'])){
            $id_jour_mardi = $_POST['id_jour_mardi'];
            $h_debut_mardi = $_POST['h_debut_mardi'];
            $h_fin_mardi = $_POST['h_fin_mardi'];
            $nbdispo = $nbdispo+1;
        }
        if(isset($_POST['id_jour_mercredi'])){
            $id_jour_mercredi = $_POST['id_jour_mercredi'];
            $h_debut_mercredi = $_POST['h_debut_mercredi'];
            $h_fin_mercredi = $_POST['h_fin_mercredi'];
            $nbdispo = $nbdispo+1;
        }
        if(isset($_POST['id_jour_jeudi'])){
            $id_jour_jeudi = $_POST['id_jour_jeudi'];
            $h_debut_jeudi = $_POST['h_debut_jeudi'];
            $h_fin_jeudi = $_POST['h_fin_jeudi'];
            $nbdispo = $nbdispo+1;
        }
        if(isset($_POST['id_jour_vendredi'])){
            $id_jour_vendredi = $_POST['id_jour_vendredi'];
            $h_debut_vendredi = $_POST['h_debut_vendredi'];
            $h_fin_vendredi = $_POST['h_fin_vendredi'];
            $nbdispo = $nbdispo+1;
        }
        if(isset($_POST['id_jour_samedi'])){
            $id_jour_samedi = $_POST['id_jour_samedi'];
            $h_debut_samedi = $_POST['h_debut_samedi'];
            $h_fin_samedi = $_POST['h_fin_samedi'];
            $nbdispo = $nbdispo+1;
        }
        if(isset($_POST['id_jour_dimanche'])){
            $id_jour_dimanche = $_POST['id_jour_dimanche'];
            $h_debut_dimanche = $_POST['h_debut_dimanche'];
            $h_fin_dimanche = $_POST['h_fin_dimanche'];
            $nbdispo = $nbdispo+1;
        }

        /*le sigle de la table type de tarification*/
        //$sigleTarifAnnonce = $TypeTarification->getTypeTarificationById($type_tarif)['sigle_type_tarif'];
        /*le lieu de la table Lieu*/
        $lieuAnnonce = $Lieu->getLieuById($ville);

    }else{
        header('Location: ./')
    }
    

    $sous_catgorie = $SousCategorie->getSousCategorieByIdSousCat($sous_cat);
    /*Scatégorie pour une annonce*/
    $CatAnnonce = $Categorie->getCategorieById($cat);
    /*Type d'annonce */
    $typeAnnonce = $TypeService->getTypeServiceById($type_service);
?>
<!DOCTYPE html>
<html  lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="../assets/images/logo2.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Prévisualiser mon annonce</title>
        <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="../assets/socicon/css/styles.css">
        <link rel="stylesheet" href="../assets/dropdown/css/style.css">
        <link rel="stylesheet" href="../assets/as-pie-progress/css/progress.min.css">
        <link rel="stylesheet" href="../assets/tether/tether.min.css">
        <link rel="stylesheet" href="../assets/theme/css/style.css">
        <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
        <style>
            .mon-overflow{
                height: 350px;
                overflow-y: scroll;
                overflow-x: hidden;
            }
            /*texte hr*/
            .mon-hr {
                /* centre verticalement les enfants entre eux */
                align-items: center;

                /* active flexbox */
                display: flex;

                /* garde le texte centré s’il passe sur plusieurs lignes ou si flexbox n’est pas supporté */
                text-align: center;
            }

            .mon-hr::before,
            .mon-hr::after {
                /* la couleur est volontairement absente ; ainsi elle sera celle du texte */
                border-top: .0625em solid;

                /* nécessaire pour afficher les pseudo-éléments */
                content: "";

                /* partage le reste de la largeur disponible */
                flex: 1;

                /* espace les traits du texte */
                margin: 0 .5em;
            }
            .cid-rVmE73Fmjs select.form-control, .cid-rVmE73Fmjs input.form-control{
              min-height: 10px;
              padding: 3px;
            }

            .cid-rVmE73Fmjs textarea.form-control{
              min-height: 152px;
            }            
        </style>
    </head>
    <body>
        <section class="menu cid-rVmtRueE1z" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="../"><img src="../assets/images/logo2.png" alt="Nom du site" style="height: 3.8rem;"></a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-4" href="../">NOM DU SITE</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="../toutes_annonces/#features1-8"><span class="mbri-bulleted-list mbr-iconfont mbr-iconfont-btn"></span>Toutes les demandes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-black display-4" href="./"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>Déposer une annonce</a>
                        </li>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../profil/">
                                    <span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span> <?php echo $user['pseudo']; ?>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/sign_up.php"><span class="mbri-plus mbr-iconfont mbr-iconfont-btn"></span>Inscription</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../authentification/login.php"><span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>Connexion</a>
                            </li>   
                        <?php endif ?>
                        <?php if ($connexion): ?>
                            <li class="nav-item">
                                <a class="nav-link link text-white display-4" href="../config/deconnexion.php"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Déconnexion</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
            </nav>
        </section>

        <section class="features1 cid-rVmE73Fmjs" id="depot_annonce">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-5">
                        <div class="form-container">
                            <div class="media-container-column" data-form-type="formoid">
                                <h1 class="mon-hr">Prévisualiser mon annonce</h1>
                                <div class="row">
                                    <form method="POST" action="./">
                                        <input type="hidden" value="<?php echo $titre_annonce ; ?>" name="lib_annon">
                                        <input type="hidden" value="<?php echo $description ; ?>" name="description">
                                        <input type="hidden" value="<?php echo $cat; ?>" name="id_cat">
                                        <input type="hidden" value="<?php echo $debut_periode ; ?>" name="dte_debut">
                                        <input type="hidden" value="<?php echo $fin_periode ; ?>" name="dte_fin">
                                        <input type="hidden" value="<?php echo $prix ; ?>" name="prix">
                                        <input type="hidden" value="<?php echo $type_tarif ; ?>" name="id_type_tarif">
                                        <input type="hidden" value="<?php echo $sous_cat ; ?>" name="id_sous_cat">
                                        <input type="hidden" value="<?php echo $tel ; ?>" name="tel">
                                        <input type="hidden" value="1" name="id_user">
                                        <input type="hidden" value="" name="id_user_reserver">
                                        <input type="hidden" value="<?php echo $type_service ; ?>" name="type_service">
                                        <input type="hidden" value="<?php echo $ville ; ?>" name="id_lieu">
                                        <input type="hidden" value="<?php echo $periode ; ?>" name="periode">
                                        <input type="hidden" value="<?php echo $j_semaine ; ?>" name="j_semaine">
                                        <input type="hidden" value="<?php echo $id_jour_lundi;?>" name="lundi">
                                        <input type="hidden" value="<?php echo $id_jour_mardi;?>" name="mardi">
                                        <input type="hidden" value="<?php echo $id_jour_mercredi;?>" name="mercredi">
                                        <input type="hidden" value="<?php echo $id_jour_jeudi;?>" name="jeudi">
                                        <input type="hidden" value="<?php echo $id_jour_vendredi;?>" name="vendredi">
                                        <input type="hidden" value="<?php echo $id_jour_samedi;?>" name="samedi">
                                        <input type="hidden" value="<?php echo $id_jour_dimanche;?>" name="dimanche">
                                        <?php 
                                            $jours_semaine = array(
                                                array('name' => 'd_lundi', 'valeur' => $h_debut_lundi),
                                                array('name' => 'd_mardi', 'valeur' => $h_debut_mardi),
                                                array('name' => 'd_mercredi', 'valeur' => $h_debut_mercredi),
                                                array('name' => 'd_jeudi', 'valeur' => $h_debut_jeudi),
                                                array('name' => 'd_vendredi', 'valeur' => $h_debut_vendredi),
                                                array('name' => 'd_samedi', 'valeur' => $h_debut_samedi),
                                                array('name' => 'd_dimanche', 'valeur' => $h_debut_dimanche),
                                                array('name' => 'f_lundi', 'valeur' => $h_fin_lundi),
                                                array('name' => 'f_mardi', 'valeur' => $h_fin_mardi),
                                                array('name' => 'f_mercredi', 'valeur' => $h_fin_mercredi),
                                                array('name' => 'f_jeudi', 'valeur' => $h_fin_jeudi),
                                                array('name' => 'f_vendredi', 'valeur' => $h_fin_vendredi),
                                                array('name' => 'f_samedi', 'valeur' => $h_fin_samedi),
                                                array('name' => 'f_dimanche', 'valeur' => $h_fin_dimanche)
                                            );

                                            for($i = 0, $size = count($jours_semaine); $i < $size; ++$i) {
                                                echo '<input type="hidden" value="'.$jours_semaine[$i]['valeur'].'" name="'.$jours_semaine[$i]['name'].'">';
                                            }
                                        ?>
                                        <button type="submit" name="modifier" class="btn btn-form btn-info display-4" id="modifier"><span class="mbri-edit2 mbr-iconfont mbr-iconfont-btn"></span> Mofidier</button>
                                    </form>
                                </div>
                                
                                <!-- Mon annonce -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <!-- Première colonne -->
                                        <div class="col-md-8 mb-5">
                                            <div class="card bg-light" >
                                                <div class="card-body border text-left">
                                                    <h1 class="card-title"> <?php echo $titre_annonce." - ".$lieuAnnonce['nom_lieu'] ; ?></h1>
                                                    <!-- Photo -->
                                                    <!-- <div class="card border" style="height: 188px;">
                                                        
                                                    </div> -->
                                                    
                                                    <!-- Button contacter -->
                                                    <!-- <div class="mt-2 mb-4">
                                                        <a href="#" class="card-link btn border p-3"><span class="mbri-letter mbr-iconfont mbr-iconfont-btn"></span> Envoyer un message</a>
                                                        <a href="#" class="card-link btn border p-3"><span class="mbri-mobile mbr-iconfont mbr-iconfont-btn"></span> Voir le numéro</a>
                                                    </div> -->

                                                    <!-- Description -->
                                                    <div class="mb-4">
                                                        <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">Description</h2>
                                                        <p class="card-text text-left">
                                                            <?php echo nl2br($description); ?>
                                                        </p>
                                                    </div>


                                                    <!-- Disponibilité -->
                                                    <!-- <div class="mb-4">
                                                        <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">Disponibilité (<?php echo $nbdispo; ?>j / 7)</h2>
                                                        <div class="table-responsive mb-2">
                                                            <table class="table table-striped table-bordered text-center">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Lundi</th>
                                                                    <th scope="col">Mardi</th>
                                                                    <th scope="col">Mercredi</th>
                                                                    <th scope="col">Jeudi</th>
                                                                    <th scope="col">Vendredi</th>
                                                                    <th scope="col">Samedi</th>
                                                                    <th scope="col">Dimanche</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><?php echo $h_debut_lundi; ?></td>
                                                                    <td><?php echo $h_debut_mardi; ?></td>
                                                                    <td><?php echo $h_debut_mercredi; ?></td>
                                                                    <td><?php echo $h_debut_jeudi; ?></td>
                                                                    <td><?php echo $h_debut_vendredi; ?></td>
                                                                    <td><?php echo $h_debut_samedi; ?></td>
                                                                    <td><?php echo $h_debut_dimanche; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><?php echo $h_fin_lundi; ?></td>
                                                                    <td><?php echo $h_fin_mardi; ?></td>
                                                                    <td><?php echo $h_fin_mercredi; ?></td>
                                                                    <td><?php echo $h_fin_jeudi; ?></td>
                                                                    <td><?php echo $h_fin_vendredi; ?></td>
                                                                    <td><?php echo $h_fin_samedi; ?></td>
                                                                    <td><?php echo $h_fin_dimanche; ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </div>

                                                        <p class="card-subtitle text-muted text-left"> 
                                                            <?php if (isset($_POST['periode'])): ?>
                                                                <span class="mbri-calendar mbr-iconfont mbr-iconfont-btn"></span> Du <?php echo dateFormatFrançais($debut_periode); ?> au <?php echo dateFormatFrançais($fin_periode); ?>
                                                            <?php endif ?>
                                                        </p>
                                                    </div> -->
                                                    <!-- Avis -->
                                                    <div class="mb-4">
                                                        <h2 class="card-subtitle mb-2 pb-2 text-muted border-bottom">Avis(0)</h2>
                                                        <p class="text-center">
                                                            <small>Soyez le premier à déposer un avis sur cette annonce.</small> <button class="border bg-light">Ajouter un avis</button>
                                                        </p>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <!-- Fin première colonne -->
                                        <!-- Deuxième colonne -->
                                        <div class="col-md-4">
                                            <div class="card bg-light mb-2" >
                                                <div class="card-body border align-center">
                                                    <span >
                                                        <img  src="https://www.mahoraisedeseaux.com/bundles/eticsitesogea/images/logo.jpg" alt="..." class="rounded-circle img-fluid border w-25 ">
                                                    </span>
                                                    
                                                    <h3><a href="#"><?php echo $user['pseudo']; ?></a></h3>
                                                </div>
                                                <div class="card-footer border">
                                                    <p class="text-left">Type d'annonce : 
                                                        <strong>
                                                            <?php echo $typeAnnonce['lib_type_service']; ?>
                                                        </strong>
                                                    </p>
                                                    <p class="text-left">Catégorie : <strong><?php echo $CatAnnonce['lib_cat']." - ".$sous_catgorie['lib_sous_cat']; ?></strong></p>
                                                    <p class="text-left">Localisation : <strong><?php echo $lieuAnnonce['nom_lieu']." (".$lieuAnnonce['cp'].")" ; ?></strong></p>
                                                    <p class="text-left">Prix : 
                                                        <strong><?php echo $prix."€"; ?>
                                                            <!-- si Une annonce est gratuit ou sur devis, on affiche que le type de tarification -->
                                                            <!-- <?php if (empty($prix)  && $type_tarif == 4 || $type_tarif == 5): ?>
                                                                <?php if ($type_tarif == 4  ): ?>
                                                                    <?php echo $sigleTarifAnnonce; ?>
                                                                <?php endif ?>
                                                                <?php if ($type_tarif == 5 ): ?>
                                                                    <?php echo $sigleTarifAnnonce; ?>
                                                                <?php endif ?>
                                                            <?php else: ?>  Sinon on affiche le prix et le type de tarification 
                                                                <?php echo $prix."€";//$sigleTarifAnnonce; ?>
                                                            <?php endif ?> -->
                                                        </strong>
                                                    </p>
                                                    <p class="text-left">Téléphone : 
                                                        <strong>
                                                            <?php echo $tel; ?>
                                                        </strong>
                                                    </p>
                                                    <p class="text-left">Publiée : <small>La date de publication n'est pas encore défini !</small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Fin deuxième colonne -->
                                    </div> 
                                </div>
                                   
                                    
                                <!---Formbuilder Form--->
                                <div class="col-md-12 ">
                                    <form action="confirm.php" method="POST" class="mbr-form form-with-styler mt-3" >
                                        <!-- Button Déposer -->
                                        <div class="row align-center">
                                            <div class="col-md-6 offset-md-3 input-group-btn">
                                                <input type="hidden" value="<?php echo $titre_annonce ; ?>" name="lib_annon">
                                                <input type="hidden" value="<?php echo $description ; ?>" name="description">
                                                <input type="hidden" value="" name="adresse">
                                                <input type="hidden" value="<?php echo $cat; ?>" name="id_cat">
                                                <input type="hidden" value="<?php echo $debut_periode ; ?>" name="dte_debut">
                                                <input type="hidden" value="<?php echo $fin_periode ; ?>" name="dte_fin">
                                                <input type="hidden" value="<?php echo $prix ; ?>" name="prix">
                                                <input type="hidden" value="<?php echo $type_tarif ; ?>" name="id_type_tarif">
                                                <input type="hidden" value="<?php echo $type_service ; ?>" name="type_service">
                                                <input type="hidden" value="<?php echo $sous_cat ; ?>" name="id_sous_cat">
                                                <input type="hidden" value="<?php echo $user['id_utilisateur']; ?>" name="id_user">
                                                <input type="hidden" value="" name="id_user_reserver">
                                                <input type="hidden" value="<?php echo $ville ; ?>" name="id_lieu">
                                                <input type="hidden" value="<?php echo $tel ; ?>" name="tel">
                                                <?php 



                                                    $jours_semaine = array(
                                                        array('name' => 'd_lundi', 'valeur' => $h_debut_lundi),
                                                        array('name' => 'd_mardi', 'valeur' => $h_debut_mardi),
                                                        array('name' => 'd_mercredi', 'valeur' => $h_debut_mercredi),
                                                        array('name' => 'd_jeudi', 'valeur' => $h_debut_jeudi),
                                                        array('name' => 'd_vendredi', 'valeur' => $h_debut_vendredi),
                                                        array('name' => 'd_samedi', 'valeur' => $h_debut_samedi),
                                                        array('name' => 'd_dimanche', 'valeur' => $h_debut_dimanche),
                                                        array('name' => 'f_lundi', 'valeur' => $h_fin_lundi),
                                                        array('name' => 'f_mardi', 'valeur' => $h_fin_mardi),
                                                        array('name' => 'f_mercredi', 'valeur' => $h_fin_mercredi),
                                                        array('name' => 'f_jeudi', 'valeur' => $h_fin_jeudi),
                                                        array('name' => 'f_vendredi', 'valeur' => $h_fin_vendredi),
                                                        array('name' => 'f_samedi', 'valeur' => $h_fin_samedi),
                                                        array('name' => 'f_dimanche', 'valeur' => $h_fin_dimanche)
                                                    );

                                                    for($i = 0, $size = count($jours_semaine); $i < $size; ++$i) {
                                                        echo '<input type="hidden" value="'.$jours_semaine[$i]['valeur'].'" name="'.$jours_semaine[$i]['name'].'">';
                                                    }
                                                ?>
                                                <button type="submit" name="deposer" class="btn btn-form btn-info display-4" id="deposer">Déposer mon annonce</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                
                    </div>
                </div>
            </div>
        </section>

        <!-- Section footer -->
        <?php 
            require_once "../footer/footer_sous-racine.php";
        ?>
        <!-- Fin section footer -->
        <script src="../assets/web/assets/jquery/jquery.min.js"></script>
        <script src="../assets/popper/popper.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/smoothscroll/smooth-scroll.js"></script>
        <script src="../assets/dropdown/js/nav-dropdown.js"></script>
        <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="../assets/parallax/jarallax.min.js"></script>
        <script src="../assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
        <script src="../assets/mbr-flip-card/mbr-flip-card.js"></script>
        <script src="../assets/tether/tether.min.js"></script>
        <script type="text/javascript" src="../assets/monjs/disponibilite.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript">
            $('#deposer').click(function(){
                swal("Annonce déposé !", "redirection en cours ...!", "success");
            })
        </script>
    </body>
</html>
