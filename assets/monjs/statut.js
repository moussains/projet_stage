$(function(){
	/*Les variables : le bloc et le select et les inputs*/
	var blocsiret = $('#blocsiret'), 
	statut = $('#statut'),
	siret = $('#siret'),
	raison_sociale = $('#raison_sociale');

	/* On cache le bloc */
	blocsiret.hide();
	/*On supprime l'attributs des vérifications sur le deux champs "n°siret et raison sociale" */
	siret.removeAttr('required');
	raison_sociale.removeAttr('required');

	/* Changement de selection */
	statut.change(function(){
		/* Si on choisi independant, association ou societe*/
		if ($(this).val()=='independant' || $(this).val()=='association' || $(this).val()=='societe') {
			/*On affiche le bloc*/
			blocsiret.show();
			/*On attribue les des vérifications des champs*/
			siret.attr('required','required');
			raison_sociale.attr('required','required');

		}else{//Sinon rien
			blocsiret.hide();
			siret.removeAttr('required');
			raison_sociale.removeAttr('required');
			var text = "";
			siret.val(text);
		}

	})
		
})