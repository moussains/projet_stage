$(function(){

	var mail = $('#mail'), mdp = $('#pwd');

	var msg_erreur_mail = $('#erreur_mail'), msg_erreur_mdp = $('#erreur_pwd'), msg_erreur_connexion = $('#erreurConnexion');

	$('#seconnecter').click(function(){
		$.get(
			"verif_donneesConnexion.php",
			{
				lemail : mail.val(),
				lemdp : mdp.val()
			},
			function(data){
				var obj = JSON.parse(data);

				msg_erreur_mdp.empty();
				msg_erreur_mail.empty();
				msg_erreur_connexion.empty();

				mail.css("border-style","none");
				mdp.css("border-style","none");

				if(obj.erreur == 1){

					msg_erreur_mdp.empty().append(obj.message);
					mdp.css("border","1px solid red");

				}else if(obj.erreur == 2){

					msg_erreur_mdp.empty().append(obj.message);
					msg_erreur_mail.empty().append(obj.message);
					msg_erreur_connexion.empty().append("Votre adresse e-mail n\'existe pas dans notre base de données ! Veuillez <a href='sign_up.php'>créer un compte</a> !");
					mail.css("border","1px solid red");
					mdp.css("border","1px solid red");

				}else{
					swal(obj.message, "Heureux de vous revoir "+obj.user_name.toUpperCase()+" !", "success");
					$('head').append('<meta http-equiv="refresh" content="2;URL=../">');
				}
			}
		);
	})

});