<!DOCTYPE html>
<html>
<head>
  	<title>Politique vie privée</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<header class="page-title" style="margin-bottom: 0px; clear: both; color: rgb(75, 75, 75); font-family: &quot;FS Me Web Regular&quot;, Helvetica, Arial, Verdana, sans-serif; font-size: 15.12px;">
	<h1 style="font-size: 4rem; margin-top: 30px; margin-bottom: 0px; line-height: 4.5rem;">Politique de respect de la vie privée</h1>
</header>
<div class="furniture" style="margin-top: 30px; color: rgb(75, 75, 75); font-family: &quot;FS Me Web Regular&quot;, Helvetica, Arial, Verdana, sans-serif; font-size: 15.12px;">
	<div class="freeform text">
		<div>
			Cette politique s’appuie sur notre mission fondamentale qui vise à offrir à nos visiteurs une information et des ressources de qualité tout en préservant l’anonymat individuel et le respect de la vie privée. La déclaration qui suit détermine la politique de confidentialité de l’Organisation internationale du Travail (OIT). Elle s’applique à tous les «sites web de l’OIT», c’est-à-dire tous les sites référencés sous le nom de domaine «ilo.org».
		</div>
		<div>
			&nbsp;
		</div>
		<h2 style="color: rgb(250, 60, 75); line-height: 1.25em;">Collecte des données personnelles des internautes par le BIT</h2>
		<div>
			Quand vous accédez à nos pages web, les informations vous concernant, ainsi que les informations techniques essentielles et non-essentielles listées ci-dessous, sont automatiquement collectées – nous les désignons globalement sous la dénomination «informations de connexion». Aucune autre information n’est collectée par le site web de l’OIT, sauf si vous décidez délibérément de nous la communiquer (par exemple, en cliquant sur un lien pour nous envoyer un courriel). L’information que vous décidez de nous transmettre éventuellement est décrite ci-dessous dans la rubrique «renseignements facultatifs».
		</div>
		<div>
			&nbsp;
		</div>
		<div>
			Informations de connexion automatiquement collectées
		</div>
		<ol style="border: none; padding: 0px; margin-left: 28px;">
			<li style="border-bottom: none; list-style: decimal !important;">Informations sur l’internaute: adresse et domaine Internet de l’ordinateur depuis lequel vous êtes connecté.</li>
			<li style="border-bottom: none; list-style: decimal !important;">Informations techniques essentielles: identification de la page ou du service que vous demandez, du type de navigateur et de système d’exploitation que vous utilisez; la date et l’heure de connexion.</li>
			<li style="border-bottom: none; list-style: decimal !important;">Informations techniques non-essentielles: l’adresse Internet du site à partir duquel vous avez été dirigé directement vers notre site web et les «cookies» (témoins de connexion) décrits ci-dessous.</li>
		</ol>
		<div>
			&nbsp;
		</div>
		<div>
			Renseignements facultatifs
		</div>
		<ol style="border: none; padding: 0px; margin-left: 28px;">
			<li style="border-bottom: none; list-style: decimal !important;">Quand vous nous envoyez un courriel: vos nom et adresse électronique, ainsi que le contenu de votre courriel.</li>
			<li style="border-bottom: none; list-style: decimal !important;">Quand vous remplissez des formulaires en ligne: toutes les données que vous décidez de saisir ou que vous confirmez.</li>
		</ol>
		<div>
			&nbsp;
		</div>
		<div>
			Le choix de fournir des informations vous appartient.
		</div>
		<div>
			Il n’existe aucune obligation juridique vous contraignant à nous fournir des informations sur notre site Web. Cependant, notre site ne peut fonctionner sans informations de routage ni données techniques essentielles. Si votre navigateur n’est pas en mesure de fournir les informations techniques non essentielles, cela ne vous empêchera pas d’utiliser notre site Web mais cela pourra nuire à la bonne marche de certaines fonctionnalités. Si vous ne fournissez pas une information facultative demandée par notre site, vous ne pourrez pas accéder à l’élément ou au service lié à cette page web.
			<br>
			&nbsp;
		</div>
		<div>
			Utilisation des cookies
		</div>
		<div>
			Le site web de l’OIT utilise des «cookies»: ce sont de petits fichiers de données transmis par un site web au disque dur de votre ordinateur pour collecter des données anonymes de navigation. Nous envoyons des cookies quand vous surfez sur&nbsp;
			<a href="https://www.ilo.org/" style="color: rgb(69, 88, 179);">www.ilo.org&nbsp;<span class="linked icon-angle-right" style="font-family: icons; speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 1; -webkit-font-smoothing: antialiased; top: 1px; position: relative; display: inline; height: 6px; font-size: 14.364px; margin-top: -1px; padding-right: 7px;"></span></a>
			, quand vous répondez à des enquêtes ou sondages en ligne ou quand vous demandez des renseignements. Le fait d’accepter les cookies que nous utilisons sur notre site ne nous donne pas accès à vos informations d’identification personnelle mais nous les utilisons pour reconnaître votre ordinateur. L’ensemble des informations collectées nous permet d’analyser les schémas de navigation sur notre site, ce qui contribue à améliorer son contenu et à rendre son utilisation plus facile.
			<br>
			&nbsp;
		</div>
		<h2 style="color: rgb(250, 60, 75); line-height: 1.25em;">Quel usage faisons-nous de ces informations?</h2>
		<div>
			Les informations relatives à l’internaute sont utilisées pour vous permettre d’afficher la page web demandée sur votre écran d’ordinateur. En théorie, la page web demandée et les informations de routage peuvent aussi être discernées par les autres entités impliquées dans la transmission de la page demandée. Nous ne contrôlons pas les pratiques de confidentialité de ces entités. Les informations techniques essentielles et non-essentielles nous aident à répondre à votre demande dans un format approprié (ou de façon personnalisée) et nous aident à prévoir des améliorations du site. Les informations facultatives nous permettent d’offrir des services ou des informations spécialement adaptés à vos besoins ou de faire suivre votre message ou votre requête auprès d’une autre entité mieux placée pour les traiter; elles nous permettent aussi de perfectionner notre site web.
		</div>
		<div>
			&nbsp;
		</div>
		<div>
			Après la transmission d’une page web, nous conservons indéfiniment les informations de connexion dans nos systèmes, mais nous ne cherchons à obtenir aucune information en vue de les relier aux personnes qui naviguent sur notre site web. Cependant, exceptionnellement, si un «pirate» tente de violer la sécurité informatique, les journaux d’informations de connexion sont gardés afin de permettre une enquête de sécurité le cas échéant et d’être transmis avec les autres informations pertinentes en notre possession aux organismes chargés de faire respecter la loi.
		</div>
		<div>
			&nbsp;
		</div>
		<div>
			Nous n’utilisons les informations que vous nous transmettez à votre sujet que pour répondre à votre requête spécifique. Nous ne partageons pas ces informations avec des tiers, sauf si cela est nécessaire pour satisfaire votre demande. De la même manière, nous n’utilisons les informations que vous nous transmettez au sujet d’un tiers lors d’une requête que pour y répondre. Une fois encore, nous ne partageons pas ces informations avec des tiers sauf si cela est nécessaire pour satisfaire ladite requête.
			<br>
			&nbsp;
		</div>
		<div>
			Généralement, nous utilisons les adresses courriels de retour dans le seul but de répondre aux courriels que nous recevons. Ces adresses ne sont utilisées à aucune autre fin.
		</div>
		<div>
			&nbsp;
		</div>
		<div>
			Enfin, nous n’utilisons jamais ni ne partageons les informations personnellement identifiables qui nous sont communiquées en ligne à des fins non conformes à celles qui sont décrites ci-dessus sans un avertissement clair sur le site concerné et sans vous donner l’occasion de décliner ou de refuser ces utilisations.
			<br>
			&nbsp;
		</div>
		<h2 style="color: rgb(250, 60, 75); line-height: 1.25em;">Notre engagement en faveur de la sécurité des données</h2>
		<div>
			L’OIT ne vend aucune information personnellement identifiable communiquée sur le site de l’OIT à aucune tierce partie. Pour éviter tout accès non autorisé, préserver la validité des données et garantir un bon usage des informations, nous avons mis en place des procédures physiques, électroniques et de gestion appropriées afin de sauvegarder et sécuriser les informations collectées en ligne, en conformité avec les politiques de l’OIT.
		</div>
		<div>
			<br>
			Tous les employés de l’OIT qui ont accès ou qui sont associés à la gestion des données personnelles sont dans l’obligation de respecter la confidentialité de tout ce qui concerne leurs fonctions officielles, y compris les données personnelles.
		</div>
		<div>
			&nbsp;
		</div>
		<h2 style="color: rgb(250, 60, 75); line-height: 1.25em;">Nous contacter</h2>
		Pour toute autre question ou interrogation relative aux politiques et pratiques de respect de la vie privée, veuillez nous envoyer un courriel à&nbsp;
	</div>
</div>

</body>
</html>